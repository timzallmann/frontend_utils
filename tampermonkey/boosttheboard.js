// ==UserScript==
// @name         Boost the Board
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Boosts functionality of our GitLab User Boards
// @author       Tim Z
// @match        https://gitlab.com/groups/gitlab-org/-/boards/*
// @grant        none
// @run-at       document-idle
// ==/UserScript==

(function() {
    'use strict';

    setTimeout(() => {
        const checkInterval = setInterval(() => {
            const checkLoaders = document.querySelectorAll('.boards-list .loading-container');
            console.log('CHECK : ', checkLoaders);
            // Sometimes the Open/Closed Column doesn't load anything due to 500 so waiting until only 1 is left
            if (checkLoaders.length <= 2) {
                console.log('NO MORE LOADERs');
                clearInterval(checkInterval);
                setTimeout(() => {boostTheBoard();},400);
            }
        }, 300);
    }, 1000);
})();

const boostTheBoard = () => {
    // Your code here...
    console.log('PIMPING STARTED ',document.querySelectorAll('.board-card'));

    let openCount = 0;
    let inDevCount = 0;
    let inReviewCount = 0;
    let doneCount = 0;

    document.querySelectorAll('.board').forEach((board) => {
        const boardTitle = board.querySelector('.board-title-text').innerText;
        console.log('TITLE: ' + board.querySelector('.board-title-text').innerText);


            board.querySelectorAll('.board-card').forEach((card) => {
                console.log('CA' , card);
                let hasUX = false;
                let isUXReady = false;
                let cardState = '';

                const removeLabels = ['frontend','feature proposal', 'Deliverable', 'devops', 'Quality', 'GitLab Ultimate', 'GitLab Starter', 'UX debt'];

                card.querySelectorAll('.board-card-labels button').forEach((label) => {
                    console.log('LA : ' + label.innerText + ' ' + removeLabels.indexOf(label));
                    if (label.innerText === 'UX') {
                        hasUX = true;
                        label.remove();
                    } else if (label.innerText === 'UX ready') {
                        isUXReady = true;
                        label.remove();
                    } else if (label.innerText === 'In review') {
                        cardState = 'review';
                        label.remove();
                    } else if (label.innerText === 'In dev') {
                        cardState = 'dev';
                        label.remove();
                    } else if (removeLabels.indexOf(label.innerText) > -1) {
                        label.remove();
                    }
                });

                if (hasUX && !isUXReady) {
                    card.style.background = '#F8E9D2';
                }

                if (boardTitle!=='Closed') {
                    let bColor = 'red';
                    if (cardState === 'dev') {
                        bColor = '#407CBF';
                        inDevCount++;
                    } else if (cardState === 'review') {
                        bColor = '#6FBA72';
                        inReviewCount++;
                    } else {
                        openCount++;
                    }
                    card.style.borderLeft = `solid 6px ${bColor}`;
                } else {
                    card.style.borderLeft = `solid 6px green`;
                }

            });
        if (boardTitle==='Closed') {
            doneCount = Number(board.querySelector('.issue-count-badge-count').innerText);
        }

    });

    document.querySelector('.js-title-container').innerHTML += `<h4>${openCount} Open -> ${inDevCount} In Dev -> ${inReviewCount} In Review -> ${doneCount} Done</h4>`;
}